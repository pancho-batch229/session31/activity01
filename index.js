/*

1. What built-in JS keyword is used to import packages?

-- require/request

2. What Node.js module/package contains a method for server creation?

-- http module (var server = http)

3. What is the method of the http object responsible for creating a server using Node.js?

-- createServer

4. Between server and client, which creates a request?

-- The client issues a request to the server.

5. Between server and client, which creates a response?

-- The server processes the request then sends a response.

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?

-- node.js

7.What is the largest registry for Node packages?

-- npm (node package manager)

*/